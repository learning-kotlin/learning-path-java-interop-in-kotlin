package org.elu.kotlin.learning.interop;

import org.elu.kotlin.learning.interop.kotlin.CustomerKotlin;
import org.elu.kotlin.learning.interop.kotlin.CustomerKotlinKt;
import org.elu.kotlin.learning.interop.kotlin.Status;
import org.elu.kotlin.learning.interop.kotlin.TopLevelFunctionsKt;
import org.elu.kotlin.learning.interop.kotlin.UtilityClass;

import java.io.IOException;

public class TalkingToKotlin {

    public void loadStats(CustomerKotlin customerKotlin) {
        try {
            customerKotlin.loadStatistics("filename");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        CustomerKotlin customerKotlin = new CustomerKotlin(5, "Edu", "edu@mail.com", null);

        customerKotlin.setEmail("mail@edu.com");

        System.out.println(customerKotlin.getSomeProperty());
        System.out.println(customerKotlin.someField);

        customerKotlin.changeStatus(Status.Current);
        customerKotlin.changeStatus();

        customerKotlin.preferential();

        // top level function from Kotlin
        System.out.println(TopLevelFunctionsKt.prefix("some", "value"));
        System.out.println(UtilityClass.prefixOther("another", "value"));
        System.out.println(UtilityClass.CopyrightYear);
        System.out.println(UtilityClass.getCopyright());

        // accessing extension functions
        CustomerKotlinKt.extension(customerKotlin);
        CustomerKotlinKt.anotherExtension(customerKotlin, "value");
    }
}
