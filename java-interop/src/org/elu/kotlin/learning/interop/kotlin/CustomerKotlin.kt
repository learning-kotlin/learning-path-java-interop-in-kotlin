package org.elu.kotlin.learning.interop.kotlin

import java.io.IOException

data class CustomerKotlin(var id: Int, var name: String, var email: String, val phone: String?) {
    val someProperty = "Value"
    @JvmField val someField = "Another Value"

    override fun toString(): String {
        return "{\"id\": \"$id\", \"name\": \"$name\", \"email\": \"$email\"}"
    }

    // to create overloaded methods from single function
    @JvmOverloads fun changeStatus(status: Status = Status.Current) {}

    @JvmName("preferential") fun makePreferred() {}

    @Throws(IOException::class) fun loadStatistics(filename: String) {
        if (filename == "") {
            throw IOException("Filename cannot be blank")
        }
    }
}

fun CustomerKotlin.extension() {}
fun CustomerKotlin.anotherExtension(value: String) {}

enum class Status {
    Current,
    Past
}
