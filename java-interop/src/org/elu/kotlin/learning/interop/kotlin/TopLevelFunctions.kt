package org.elu.kotlin.learning.interop.kotlin

fun prefix(prefix: String, value: String): String {
    return "$prefix-$value"
}
