@file:JvmName("UtilityClass")

package org.elu.kotlin.learning.interop.kotlin

// this is converted to constant
const val CopyrightYear = 2018
// this is converted to read only property with getter method
val Copyright = "Edu"

fun prefixOther(prefix: String, value: String): String {
    return "$prefix-$value"
}
