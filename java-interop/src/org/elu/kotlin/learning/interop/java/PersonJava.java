package org.elu.kotlin.learning.interop.java;

public class PersonJava {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
