package org.elu.kotlin.learning.interop

import org.elu.kotlin.learning.interop.java.CustomerJava
import org.elu.kotlin.learning.interop.java.CustomerRepository
import org.elu.kotlin.learning.interop.java.PersonJava

fun main(args: Array<String>) {
    val customer = CustomerJava()
    customer.id = 5
    customer.name = "Edu"
    customer.email = "edu@mail.com"
    customer.prettyPrint()

    customer.neverNull()

    // easier way to implement interface with single method
    val runnable = Runnable { println("Invoking runnable") }

    val kr = KotlinCustomerRepository()
    val customerJava = kr.getById(1)
    customerJava?.id
}

class PersonKotlin: PersonJava()

class KotlinCustomerRepository: CustomerRepository {
    // method can be defined as nullable
    override fun getById(id: Int): CustomerJava? {
        return CustomerJava()
    }
    // or if we know that it does not return null, ? mark can be removed
    override fun getAll(): MutableList<CustomerJava> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

class RunnableKotlin: Runnable {
    override fun run() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
